package org.torproject.artitoyvpn.ui.settings;

import android.app.Application;
import android.net.InetAddresses;
import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.core.util.PatternsCompat;
import androidx.lifecycle.AndroidViewModel;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.utils.PreferenceHelper;
import org.torproject.artitoyvpn.utils.Utils;
import org.torproject.onionmasq.OnionMasq;
import org.torproject.onionmasq.errors.CountryCodeException;

public class SettingsViewModel extends AndroidViewModel {

    private final PreferenceHelper preferenceHelper;
    private String turnServerHost = null;
    private long turnServerPort;
    private String turnServerAuth;


    public SettingsViewModel(Application application) {
        super(application);
        preferenceHelper = new PreferenceHelper(application);
        this.turnServerHost = preferenceHelper.getTurnServerHost();
        this.turnServerPort = preferenceHelper.getTurnServerPort();
        this.turnServerAuth = preferenceHelper.getTurnServerAuth();
    }

    public String getBridgeLines() {
        return preferenceHelper.getBridgeLines();
    }

    public void onBridgeLinesChanged(String lines) {
        if (lines.isBlank()) {
            preferenceHelper.setBridgeLines(null);
        } else {
            preferenceHelper.setBridgeLines(lines);
        }
    }

    public int getExitNodeCountryPos() {
        String cc = preferenceHelper.getExitNodeCountry();
        String[] countries = getApplication().getResources().getStringArray(R.array.countries);
        for (int i = 0; i < countries.length; i++) {
            if (countries[i].equals(cc)) {
                return i;
            }
        }
        return 0;
    }

    public
    void onCountryCodeSelected(int pos) {
        String[] countries = getApplication().getResources().getStringArray(R.array.countries);
        String country = countries[pos];
        if (country.equals("(any)")) {
            country = null;
        }
        try {
            OnionMasq.setCountryCode(country);
            preferenceHelper.setExitNodeCountry(country);
        } catch (CountryCodeException e) {
            throw new RuntimeException(e);
        }
    }

    public void resetCountryCode() {
        Toast.makeText(getApplication(), "Unsetting country.", Toast.LENGTH_SHORT).show();
        try {
            OnionMasq.setCountryCode(null);
            preferenceHelper.setExitNodeCountry(null);
        } catch (CountryCodeException e) {
            throw new RuntimeException(e);
        }
    }

    public String getTurnServer() {
        if (TextUtils.isEmpty(turnServerHost)) {
            return null;
        }
        return turnServerHost + ":" + turnServerPort;
    }

    public void onTurnServerChanged(String turnServer) throws IllegalArgumentException {
        if (TextUtils.isEmpty(turnServer)) {
            // reset turn server settings
            turnServerPort = 0;
            turnServerHost = null;
        } else {
            //rough input validation
            String[] params = turnServer.split(":");
            if (params.length != 2) {
                throw new IllegalArgumentException(turnServer + " has wrong formatting. Expected <host:port>");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // there's no good built-in IPv6 address validation for pre-Android 10, so we skip
                // the validation for the host part in older Android versions. That way we don't need
                // to integrate 3rd party libs as com.github.seancfoley.ipaddress
                if (params[0].endsWith(".onion") && !Utils.isOnionAddress(params[0])) {
                    throw new IllegalArgumentException(turnServer + " doesn't contain a valid onion address.");
                }

                if (!PatternsCompat.DOMAIN_NAME.matcher(params[0]).matches() &&
                        !InetAddresses.isNumericAddress(params[0])) {
                    throw new IllegalArgumentException(turnServer + " doesn't contain a valid host.");
                }
            }

            long port = 0;
            try {
                port = Long.parseLong(params[1]);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
                throw new IllegalArgumentException(turnServer + " doesn't contain a valid port.");
            }
            if (port < 0 || port > 65535) {
                throw new IllegalArgumentException(turnServer + " doesn't contain a valid port.");
            }

            turnServerPort = port;
            turnServerHost = params[0];
        }

        preferenceHelper.setTurnServerHost(turnServerHost);
        preferenceHelper.setTurnServerPort(turnServerPort);
        OnionMasq.setTurnServerConfig(turnServerHost, turnServerPort, turnServerAuth);
    }

    public String getTurnServerAuth() {
        return turnServerAuth;
    }

    public void onTurnServerAuthChanged(String sharedSecret) {
        turnServerAuth = sharedSecret;
        preferenceHelper.setTurnServerAuth(turnServerAuth);
        if (TextUtils.isEmpty(turnServerHost) || turnServerPort == 0) {
            return;
        }
        OnionMasq.setTurnServerConfig(turnServerHost, turnServerPort, turnServerAuth);
    }
}