package org.torproject.artitoyvpn.ui.appListing;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.torproject.artitoyvpn.databinding.FragmentCircuitDialogBinding;

public class CircuitDialogFragment extends DialogFragment {

    public static final String APP_UID = "APP_UID";
    private CircuitDialogViewModel viewModel;
    private FragmentCircuitDialogBinding binding;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        int appUID = -1;
        if (args != null) {
            appUID = args.getInt(CircuitDialogFragment.APP_UID, -1);
        }

        viewModel = new ViewModelProvider(this, new CircuitDialogViewModelFactory(getActivity().getApplication(), appUID)).get(CircuitDialogViewModel.class);
        binding = FragmentCircuitDialogBinding.inflate(getLayoutInflater());
        binding.tvCircuitDetails.setText(viewModel.circuitDetails);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Circuits")
                .setPositiveButton("OK", (dialog, btn) -> dialog.dismiss())
                .setView(binding.getRoot())
                .create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}