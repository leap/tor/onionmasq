package org.torproject.artitoyvpn;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import org.torproject.artitoyvpn.vpn.VpnStatusObservable;
import org.torproject.onionmasq.OnionMasqJni;
import org.torproject.onionmasq.errors.OnionmasqException;
import org.torproject.onionmasq.logging.LogObservable;
import org.torproject.onionmasq.OnionMasq;

import java.io.File;

public class ArtiToyVPNApp extends Application {

    VpnStatusObservable vpnStatusObservable;
    LogObservable logObservable;

    @Override
    public void onCreate() {
        super.onCreate();
        vpnStatusObservable = VpnStatusObservable.getInstance();
        logObservable = LogObservable.getInstance();
        try {
            OnionMasq.init(this);
        } catch (OnionmasqException e) {
            // FIXME(eta): better error handling here
            throw new RuntimeException(e);
        }

        // Uncomment this to enable pcap debugging.
        // FIXME(eta): This should maybe be a feature flag, or a user-configurable setting?
        // File file = new File(this.getExternalFilesDir(null), "out.pcap");
        // OnionMasqJni.setPcapPath(file.getAbsolutePath());
    }
}
