package org.torproject.onionmasq.circuit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import java.util.ArrayList;

public class CircuitCountryCodesTest {

    @Test
    public void testEqualsSameOrder() {
        ArrayList<String> listA = new ArrayList<>();
        listA.add("DE");
        listA.add("SE");
        listA.add("US");

        ArrayList<String> listB = new ArrayList<>();
        listB.add("DE");
        listB.add("SE");
        listB.add("US");
        CircuitCountryCodes ccA = new CircuitCountryCodes(listA);
        CircuitCountryCodes ccB = new CircuitCountryCodes(listB);

        assertEquals("CountryCodes in same order equal", ccA, ccB);
    }

    @Test
    public void testEqualsDifferentOrder() {
        ArrayList<String> listA = new ArrayList<>();
        listA.add("SE");
        listA.add("US");
        listA.add("DE");

        ArrayList<String> listB = new ArrayList<>();
        listB.add("DE");
        listB.add("SE");
        listB.add("US");
        CircuitCountryCodes ccA = new CircuitCountryCodes(listA);
        CircuitCountryCodes ccB = new CircuitCountryCodes(listB);

        assertNotEquals("CountryCodes in different order not equal", ccA, ccB);
    }

    @Test
    public void testEqualsDifferentelements() {
        ArrayList<String> listA = new ArrayList<>();
        listA.add("SE");
        listA.add("US");
        listA.add("NL");

        ArrayList<String> listB = new ArrayList<>();
        listB.add("SE");
        listB.add("US");
        listB.add("DE");
        CircuitCountryCodes ccA = new CircuitCountryCodes(listA);
        CircuitCountryCodes ccB = new CircuitCountryCodes(listB);

        assertNotEquals("CountryCodes different elements not equal", ccA, ccB);
    }
}