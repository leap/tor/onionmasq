package org.torproject.onionmasq;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.torproject.onionmasq.circuit.Circuit;
import org.torproject.onionmasq.events.ClosedConnectionEvent;
import org.torproject.onionmasq.events.FailedConnectionEvent;
import org.torproject.onionmasq.events.NewConnectionEvent;
import org.torproject.onionmasq.events.OnionmasqEvent;
import org.torproject.onionmasq.testhelper.TestHelper;

import java.io.IOException;
import java.util.ArrayList;

public class CircuitStoreTest {

    @Test
    public void testGetCircuitsForAppUid_deduplicate_circuits() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection2.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(event1);
        assertEquals("Multiple socket connections can be routed over the same circuit. The corresponding circuit should be de-duplicated if the tor destination is the same", 1, store.getCircuitsForAppUid(event1.appId).size());

        store.handleEvent(event2);
        assertEquals("destination www.torproject.org and torproject.org are not the same, thus 2 circuits are expected", 2, store.getCircuitsForAppUid(event1.appId).size());
    }


    @Test
    public void testGetCircuitsForAppUid_removeCircuitsAfterLastOpenSocketWasClosed() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection2.json");
        ClosedConnectionEvent closedEvent1 = (ClosedConnectionEvent) createConnectionEvent("events/closedConnection1.json");
        ClosedConnectionEvent closedEvent2 = (ClosedConnectionEvent) createConnectionEvent("events/closedConnection2.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(event1);
        store.handleEvent(event2);
        store.handleEvent(closedEvent1);
        assertEquals("A circuit should be removed from the store only if the last open socket was closed.", 1, store.getCircuitsForAppUid(event1.appId).size());

        store.handleEvent(closedEvent2);
        assertEquals("An empty Array is returned if no open socket connection for an appId is available.", 0, store.getCircuitsForAppUid(event1.appId).size());
    }

    @Test
    public void testGetCircuitsForAppUid_return_different_circuits() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection3.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(event2);
        ArrayList<Circuit> circuits = store.getCircuitsForAppUid(event1.appId);
        assertEquals("2 circuits should be returned", 2, circuits.size());
        assertNotEquals("circuits should have different addresses", circuits.get(0).getRelayDetails().get(0).addresses.get(0),  circuits.get(1).getRelayDetails().get(0).addresses.get(0));
    }

    @Test
    public void testGetCircuitsForAppUid_removeCircuitOnFailure() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection2.json");
        FailedConnectionEvent failureEvent = (FailedConnectionEvent) createConnectionEvent("events/failedConnection1.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(failureEvent);
        assertNotNull(store.getCircuitsForAppUid(event1.appId));
        assertEquals(0, store.getCircuitsForAppUid(event1.appId).size());

        store.handleEvent(event1);
        store.handleEvent(event2);
        store.handleEvent(failureEvent);
        assertEquals(1, store.getCircuitsForAppUid(event1.appId).size());
    }


    @Test
    public void testReset_removedCircuits() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection3.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(event2);
        store.reset();

        assertEquals(0, store.getCircuitsForAppUid(event1.appId).size());
    }

    @Test
    public void getCircuitCountryCodesForAppUid_deduplicateCountryCodesForSameCircuit() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection2.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        store.handleEvent(event2);

        assertEquals(event1.appId, event2.appId);
        assertEquals(1, store.getCircuitCountryCodesForAppUid(event1.appId).size());
    }

    @Test
    public void getCircuitCountryCodesForAppUid_takeOnlyTheLastCircuitIntoAccount() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");
        NewConnectionEvent event2 = (NewConnectionEvent) createConnectionEvent("events/newConnection3.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        assertEquals("GB", store.getCircuitCountryCodesForAppUid(event1.appId).get(0).getCountryCodes().get(2));

        store.handleEvent(event2);

        assertEquals(event1.appId, event2.appId);
        assertEquals(1, store.getCircuitCountryCodesForAppUid(event1.appId).size());
        assertEquals("DE", store.getCircuitCountryCodesForAppUid(event1.appId).get(0).getCountryCodes().get(2));
    }

    @Test
    public void getCircuitCountryCodesForAppUid_testCloseEventsDontRemoveCC() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        assertEquals(1, store.getCircuitCountryCodesForAppUid(event1.appId).size());

        ClosedConnectionEvent closeEvent1 = (ClosedConnectionEvent) createConnectionEvent("events/closedConnection1.json");
        store.handleEvent(closeEvent1);
        assertEquals("tracked socket connection gets removed after closing", 0, store.getCircuitsForAppUid(event1.appId).size());
        assertEquals("associated circuit country codes remain after socket connection closed", 1, store.getCircuitCountryCodesForAppUid(event1.appId).size());

    }

    @Test
    public void getCircuitCountryCodesForAppUid_testFailEventsDontRemoveCC() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        assertEquals(1, store.getCircuitCountryCodesForAppUid(event1.appId).size());

        FailedConnectionEvent failEvent = (FailedConnectionEvent) createConnectionEvent("events/failedConnection1.json");
        store.handleEvent(failEvent);
        assertEquals("tracked socket connection gets removed after fail event", 0, store.getCircuitsForAppUid(event1.appId).size());
        assertEquals("associated circuit country codes remain after socket connection closed", 1, store.getCircuitCountryCodesForAppUid(event1.appId).size());
    }

    @Test
    public void getCircuitsCountryCodesForAppUid_resetRemovesCC() throws IOException {
        NewConnectionEvent event1 = (NewConnectionEvent) createConnectionEvent("events/newConnection1.json");

        CircuitStore store = new CircuitStore();
        store.handleEvent(event1);
        assertEquals(1, store.getCircuitCountryCodesForAppUid(event1.appId).size());

        store.reset();
        assertEquals("tracked socket connection gets removed after reset", 0, store.getCircuitsForAppUid(event1.appId).size());
        assertEquals("circuit country codes get removed after reset", 0, store.getCircuitCountryCodesForAppUid(event1.appId).size());
    }


    private OnionmasqEvent createConnectionEvent(String path) throws IOException, NullPointerException {
        String json = TestHelper.getStringFromResources(path);
        return OnionmasqEvent.fromJson(json);
    }
}