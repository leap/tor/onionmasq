package org.torproject.onionmasq;

public interface ISocketProtect {
     boolean protect(int socket) ;
}
