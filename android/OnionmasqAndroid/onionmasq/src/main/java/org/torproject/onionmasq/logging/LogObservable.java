package org.torproject.onionmasq.logging;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.torproject.onionmasq.utils.Utils;

import java.util.LinkedList;
import java.util.Locale;

public class LogObservable extends ViewModel {

    private final MutableLiveData<LinkedList<LogItem>> logListData;
    private static LogObservable instance;

    private LogObservable() {
        logListData = new MutableLiveData<>(new LinkedList<>());
    }

    public static LogObservable getInstance() {
        if (instance == null) {
            instance = new LogObservable();
        }
        return instance;
    }

    public void addLog(String log) {
        LinkedList<LogItem> list = logListData.getValue();
        if (list != null) {
            list.addFirst(new LogItem(System.currentTimeMillis(), log));
            logListData.postValue(list);
        }
    }


    public String getLogStrings(boolean showTimestamp) {
        StringBuilder builder = new StringBuilder();
        LinkedList<LogItem> logItemArrayList = logListData.getValue();
        if (logItemArrayList == null) {
            return "";
        }
        for (LogItem item : logItemArrayList) {
            builder.append(item.toString(showTimestamp)).append("\n");
        }
        return builder.toString();
    }

    public LiveData<LinkedList<LogItem>> getLogListData() {
        return logListData;
    }

    public void clearLogs() {
        logListData.postValue(new LinkedList<>());
    }

}