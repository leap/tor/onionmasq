use onionmasq_pt_wrapper::{start_transport, TransportKind};
use tracing::info;
use tracing_subscriber::filter::LevelFilter;

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(LevelFilter::DEBUG)
        .init();

    start_transport(
        TransportKind::Obfs4,
        "127.0.0.1:4000".parse().unwrap(),
        "/tmp/lyrebird/",
    )
    .unwrap();
    info!("Listening on port 4000 for SOCKS connections");
    loop {
        std::thread::park()
    }
}
