//! Configuring the tunnel.

use crate::errors::{TunnelError, TunnelResult};
use crate::TunnelScaffolding;
#[cfg(feature = "pluggable-transports")]
use arti_client::config::pt::TransportConfigBuilder;
use arti_client::config::{BoolOrAuto, BridgeConfigBuilder, CfgPath, TorClientConfigBuilder};
use arti_client::TorClientConfig;
#[cfg(feature = "pluggable-transports")]
use onionmasq_pt_wrapper::TransportKind;
#[cfg(feature = "pluggable-transports")]
use std::fs;
#[cfg(feature = "udp-tunnel")]
use std::hash::{Hash, Hasher};
#[cfg(feature = "pluggable-transports")]
use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpListener};
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;
#[cfg(feature = "pluggable-transports")]
use tor_linkspec::PtTransportName;
#[cfg(feature = "pluggable-transports")]
use tracing::{debug, trace};

// Re-export Arti types used in the configuration
pub use arti_client::{IntoTorAddr, TorAddr, TorAddrError};

// Default port number for TURN servers
#[cfg(feature = "udp-tunnel")]
pub const DEFAULT_TURN_PORT: u16 = turn_tcp_client::DEFAULT_PORT;

// Re-export UDP configuration types
#[cfg(feature = "udp-tunnel")]
pub use turn_tcp_client::auth::TurnAuth;

/// Configuration for an `OnionTunnel`.
///
/// # Usage notes
///
/// This struct is marked `#[non_exhaustive]` so future fields can be added, which means it's
/// slightly harder to make one by hand. You can make use of the `Default` impl, though:
///
/// ```rust
/// use onion_tunnel::config::TunnelConfig;
///
/// let mut cfg = TunnelConfig::default();
/// // for example
/// cfg.state_dir = Some("/path/to/state".into());
/// ```
#[derive(Clone, Debug, PartialEq, Default)]
#[non_exhaustive]
pub struct TunnelConfig {
    /// Where to store Arti's persistent state.
    pub state_dir: Option<PathBuf>,
    /// Where to store Arti's cache.
    pub cache_dir: Option<PathBuf>,
    /// Where to store persistent data for Pluggable Transports.
    ///
    /// If not specified, a subdirectory of `state_dir` (or its default value)
    /// will be used.
    pub pt_dir: Option<PathBuf>,
    /// A list of bridge lines to use.
    ///
    /// If this is empty, bridges will be disabled.
    pub bridge_lines: Vec<String>,
    /// A path to write a pcap file to for debugging.
    pub pcap_path: Option<PathBuf>,
}

impl TunnelConfig {
    /// Make an Arti `TorClientConfig` using the values in this `TunnelConfig`.
    pub(crate) fn arti_config(
        &self,
        _scaffolding: Arc<impl TunnelScaffolding>,
    ) -> TunnelResult<TorClientConfig> {
        let mut builder = TorClientConfigBuilder::default();

        if let Some(ref state_dir) = self.state_dir {
            builder
                .storage()
                .state_dir(CfgPath::new_literal(state_dir.to_owned()));
        }

        if let Some(ref cache_dir) = self.cache_dir {
            builder
                .storage()
                .cache_dir(CfgPath::new_literal(cache_dir.to_owned()));
        }

        if !self.bridge_lines.is_empty() {
            for line in self.bridge_lines.iter() {
                let bcb =
                    BridgeConfigBuilder::from_str(line).map_err(TunnelError::BadBridgeLine)?;

                builder.bridges().bridges().push(bcb);
            }
            builder.bridges().enabled(BoolOrAuto::Explicit(true));
        }

        #[cfg(feature = "pluggable-transports")]
        {
            trace!("Attempting to enable pluggable transports.");

            let pt_scaff = _scaffolding.clone();
            onionmasq_pt_wrapper::set_protect_callback(move |fd| {
                debug!("PT protect() for {fd}");
                // FIXME(eta): either provide the actual address here, or just change the interface.
                //             I don't think this is actually used any more now that we have the
                //             whole fwmark solution thing.
                let fake = SocketAddr::new(IpAddr::V4(Ipv4Addr::UNSPECIFIED), 0);
                pt_scaff.protect(fd, &fake)
            });

            let pt_dir = match self.pt_dir {
                Some(ref v) => v.clone(),
                None => {
                    if let Some(ref state_dir) = self.state_dir {
                        state_dir.join("pt_state")
                    } else {
                        // Attempt to figure out the Arti state directory.
                        let state_dir = CfgPath::new("${ARTI_LOCAL_DATA}".into())
                            .path()
                            .map_err(TunnelError::ArtiDefaultState)?;
                        state_dir.join("pt_state")
                    }
                }
            };
            fs::create_dir_all(&pt_dir).map_err(|e| TunnelError::Directory {
                dir: pt_dir.clone(),
                err: e,
            })?;
            let pt_dir = pt_dir.to_string_lossy();

            // HACK(eta): Find a free port by binding a TCP listener, getting the port, and then
            //            immediately dropping it and passing that address to the PT.
            //            This is very racy, but we don't have a way to get the bound port out of
            //            the PT yet.
            let listener = TcpListener::bind("127.0.0.1:0").map_err(TunnelError::PtSetup)?;
            let socket_addr = listener.local_addr().map_err(TunnelError::PtSetup)?;
            drop(listener);
            onionmasq_pt_wrapper::start_transport(TransportKind::Obfs4, socket_addr, &pt_dir)
                .map_err(TunnelError::PtSetupGo)?;

            let mut tcb = TransportConfigBuilder::default();

            #[allow(clippy::unwrap_used)]
            tcb.protocols(vec![PtTransportName::from_str("obfs4").unwrap()])
                .proxy_addr(socket_addr);

            builder.bridges().transports().push(tcb);
            trace!("Registered obfs4 at {socket_addr}");
        }

        builder.build().map_err(TunnelError::ArtiConfig)
    }
}

/// TURN server configuration, for UDP tunneling.
#[cfg(feature = "udp-tunnel")]
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TurnConfig {
    pub tor_addr: TorAddr,
    pub auth: TurnAuth,
}

#[cfg(feature = "udp-tunnel")]
impl TurnConfig {
    /// Create a new default TURN configuration with the given server string
    ///
    /// The address will be converted to a [`TorAddr`], using a default port
    /// number if none was explicitly given.
    ///
    pub fn new(addr: &str) -> Result<Self, TorAddrError> {
        Ok(Self {
            tor_addr: match addr.into_tor_addr() {
                Ok(tor_addr) => tor_addr,
                Err(TorAddrError::NoPort) => (addr, DEFAULT_TURN_PORT).into_tor_addr()?,
                Err(e) => return Err(e),
            },
            auth: TurnAuth::None,
        })
    }
}

#[cfg(feature = "udp-tunnel")]
impl Hash for TurnConfig {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.tor_addr.to_string().hash(state);
        self.auth.hash(state);
    }
}
