//! Error types for the `turn-tcp-client`

/// Top-level error type for `turn-tcp-client`
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("{0}")]
    Turn(#[from] turn::Error),

    #[error("Unsuccessful STUN transaction")]
    UnsuccessfulStunTransaction,

    #[error("Datagram would be too large to encode")]
    DatagramTooLarge,

    #[error("The server requires authorization, but no method was provided")]
    AuthRequired,

    #[error("Too many concurrent peers")]
    TooManyPeers,

    #[error("Allocation has expired")]
    AllocationExpired,
}

impl From<stun::Error> for Error {
    fn from(stun: stun::Error) -> Self {
        turn::Error::Stun(stun).into()
    }
}

impl From<std::io::Error> for Error {
    fn from(io: std::io::Error) -> Self {
        turn::Error::Io(io.into()).into()
    }
}
