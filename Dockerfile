# Build a build-environment image which is used by CI, and optionally by developers.
# The output of this build ends up at "registry.0xacab.org/leap/tor/onionmasq".
# This does not build the onionmasq project itself.

FROM containers.torproject.org/tpo/tpa/base-images:bookworm@sha256:0489265560bef9cfb22860c949c56d833a3f9831cc25fd4cfbec4b52c72c200d

ENV JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64 \
    ANDROID_HOME="${PWD}/android-home" \
    ANDROID_COMPILE_SDK="34" \
    ANDROID_BUILD_TOOLS="34.0.0" \
    ANDROID_SDK_TOOLS="10406996" \
    ANDROID_NDK_VERSION="25.2.9519653" \
    ANDROID_NDK_SHA256="769ee342ea75f80619d985c2da990c48b3d8eaf45f48783a2d48870d04b46108" \
    ANDROID_CLT_SHA256="8919e8752979db73d8321e9babe2caedcc393750817c1a5f56c128ec442fb540" \
    ANDROID_NDK_RELNAME="r25c" \
    GOLANG_VERSION="1.21.1" \
    GOLANG_SHA256="b3075ae1ce5dab85f89bc7905d1632de23ca196bd8336afd93fa97434cfa55ae"

# Computed NDK paths based on ANDROID_NDK_VERSION
ENV ANDROID_NDK_HOME="${ANDROID_HOME}/ndk/${ANDROID_NDK_VERSION}" \
    LLVM_PREBUILD="${ANDROID_NDK_VERSION}/toolchains/llvm/prebuilt/linux-x86_64/bin"
ENV ANDROID_NDK="${ANDROID_NDK_HOME}"

# Search paths
ENV PATH="/root/.cargo/bin:/usr/local/go/bin:${JAVA_HOME}/bin:${ANDROID_NDK}:${ANDROID_HOME}/cmdline-tools/cmdline-tools/bin/:${PATH}"

# Update and configure Debian
RUN apt-get clean && \
    apt-get -yq update && \
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -yq install tzdata && \
    apt-get -yq upgrade

# Workaround for a dpkg configuration failure (openjdk-17-jre-headless without manpage directory)
RUN mkdir -p /usr/share/man/man1 && \
    DEBIAN_FRONTEND=noninteractive apt-get -yq install \
    openjdk-17-jre-headless

# Install remaining package dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install \
    apt-utils wget tar wget tar unzip curl git pkg-config \
    build-essential openjdk-17-jdk \
    lib32stdc++6 lib32z1 libssl-dev libclang-dev

# Get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

RUN rustup install "stable" \
    && rustup default stable \
    && rustup target add armv7-linux-androideabi aarch64-linux-android i686-linux-android x86_64-linux-android \
    && rustup show

# Get Android ndk
RUN install -d $ANDROID_HOME \
    && cd $ANDROID_HOME \
    && wget -nv --output-document=ndk.zip https://dl.google.com/android/repository/android-ndk-${ANDROID_NDK_RELNAME}-linux.zip \
    && echo "${ANDROID_NDK_SHA256} ndk.zip" | sha256sum --strict -c - \
    && unzip -qq -d ndk ndk.zip \
    && rm -f ndk.zip \
    && mv ndk/android-ndk-${ANDROID_NDK_RELNAME}/ ndk/${ANDROID_NDK_VERSION}/ \
    && cd ..

# Get Android sdk
RUN wget -nv --output-document=${ANDROID_HOME}/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip \
    && cd $ANDROID_HOME \
    && echo "${ANDROID_CLT_SHA256} cmdline-tools.zip" | sha256sum --strict -c - \
    && unzip -qq -d cmdline-tools cmdline-tools.zip \
    && rm -f cmdline-tools.zip \
    && cd ..

# Accepting licences before using sdkmanager
RUN yes | sdkmanager --licenses \
    && sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-${ANDROID_COMPILE_SDK}" \
    && sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" \
    && sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${ANDROID_BUILD_TOOLS}"

# Install Go
RUN wget -nv https://go.dev/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz \
    && echo "${GOLANG_SHA256} go${GOLANG_VERSION}.linux-amd64.tar.gz" | sha256sum --strict -c - \
    && tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz \
    && rm -f go${GOLANG_VERSION}.linux-amd64.tar.gz

# Install cargo-ndk
RUN cargo install cargo-ndk --version 2.12.6
